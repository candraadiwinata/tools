<?php include "koneksi.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ORDER | TOOLS</title>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

					<?php 
					$id_tool = isset($_POST['id_tool'])?$_POST['id_tool']:NULL;
					$nik = isset($_POST['nik'])?$_POST['nik']:NULL;
					$unit = isset($_POST['unit'])?$_POST['unit']:NULL;
					$jml = isset($_POST['jml'])?$_POST['jml']:NULL;
					$qty = isset($_POST['qty'])?$_POST['qty']:NULL;
					$karyawanhd = isset($_POST['karyawanhd'])?$_POST['karyawanhd']:NULL;
					$unithd = isset($_POST['unithd'])?$_POST['unithd']:NULL;
					$pesanhd = isset($_POST['pesanhd'])?$_POST['pesanhd']:NULL;
					$tgl = date('Y-m-d H:i:s');
					$int = (int)$qty;
					$hasil = $int - $jml;
					if(isset($_POST['btn'])?$_POST['btn']:NULL){
					$query = mysqli_query($konek, "INSERT INTO `out_tools`(`id_outtool`, `karyawan`, `unit`, `id_tool`, `outtool_qty`, `outtool_date`, `stts`) VALUES ('', '$nik', '$unit', '$id_tool', '$jml', '$tgl', 1)");
					$query2 = mysqli_query($konek, "UPDATE tools SET tool_qty='$hasil' WHERE id_tool='$id_tool'"); 
					if ($query): header("Refresh:5; url=index.php"); ?>
						<div class="alert alert-success" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<?php echo "Konfirmasi telah terkirim ke admin,  silahkan ambil Tools yang anda order !"; ?>
						</div>
					<?php endif; 
					}
					if(isset($_POST['bantuan'])?$_POST['bantuan']:NULL){
					$query = mysqli_query($konek, "INSERT INTO `helpdesktools`(`id_hdtools`, `karyawan`, `unit`, `pesanhd`, `balasan`, `status`) VALUES ('', '$karyawanhd', '$unithd', '$pesanhd', '', 1)");
					if ($query): header("Refresh:5; url=index.php"); ?>
						<div class="alert alert-success" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<?php echo "Pesan anda telah terkirim ke admin, silahkan tunggu balasan dari admin !"; ?>
						</div>
					<?php endif; 
					}
					$q = mysqli_query($konek, "SELECT * FROM helpdesktools WHERE status=0");
					if($q):
					while($pesan=mysqli_fetch_array($q)){
					?>
						<div class="alert alert-info" role="alert">
							Admin : <?php echo $pesan['balasan']; ?></br>
						</div>
					
					<?php } endif; ?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">TOOLS STOCK (HOUSEKEEPING)</h3>
				<span class="float-right">&nbsp;
				<a title="Cetak Monthly Report" onclick="helpDesk()" class="btn btn-success btn-small float-right" href="#!"><i class="fas fa-comments"> Pesan</i></a>
				</span>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				<div class="table-responsive">
				<table class="table table-hover table-bordered table-sm table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Stock</th>
                    <th>Status</th>
                    <th>Order</th>
                  </tr>
                  </thead>
                  <tbody>
				  <?php 
				  $no = 1;
					$sql=mysqli_query($konek, "SELECT * FROM tools ORDER BY tool_name ASC");
					while($d=mysqli_fetch_array($sql)){
						if($d['tool_qty']>0){$stts="Available"; $bg=NULL;  $klik="onclick";}else{$stts="Empty"; $bg="pink"; $klik="";}
						echo "
						<tr>
							<td bgcolor='$bg' width='40px' align='center'>$no</td>
							<td bgcolor='$bg'>$d[tool_name]</td>
							<td bgcolor='$bg'>$d[tool_qty]</td>
							<td bgcolor='$bg'>$stts</td>
							<td bgcolor='$bg' width='40px' align='center'><center>
							<a title='Order Tools' $klik='orderTools($d[id_tool])' class='btn btn-info btn-small float-right' href='#'><i class='fas fa-shopping-cart'> </i></a>
							</center></td>
						</tr>
						";
						$no++;
					}
					?>
                  </tfoot>
                </table>
              </div>
			  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
	<center>
      <b>FID</b>
    <strong>Copyright &copy; 2021 <a href="#">IndofoodCBP</a>.</strong> All rights reserved. <a onclick="helpDesk()" href="#">Help Desk Respons</a>
	</center>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="judul" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title" id="judul"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
            <div id="tampil_modal">
            <!-- Data akan di tampilkan disini-->
            </div>
        </div>
      </div>
    </div>
</div>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="assets/datatables/jquery.dataTables.js"></script>
<script src="assets/datatables/dataTables.bootstrap4.js"></script>
<script src="js/demo/datatables-demo.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
	function orderTools(id){
		var id_tool = id;
		const link = 'order.php';
			$.ajax({
				url: link,
				method: 'post',
				data: {id_tool:id_tool},
				success:function(data){
					$('#modal-default').modal("show");
					$('#tampil_modal').html(data);
					document.getElementById("judul").innerHTML='Order Tools'; 
				}
            });
    }
	function helpDesk(id){
		var id_tool = id;
		const link = 'helpdesk.php';
			$.ajax({
				url: link,
				method: 'post',
				data: {id_tool:id_tool},
				success:function(data){
					$('#modal-default').modal("show");
					$('#tampil_modal').html(data);
					document.getElementById("judul").innerHTML='Helpdesk Form'; 
				}
            });
    }
</script>
</body>
</html>
