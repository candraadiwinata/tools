			</div>
			</section>
			
		</div>
<!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="http://10.126.24.221//FID-PRODUCTION/">Food Ingredient Division - Production</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.1.1-ca
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="judul" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title" id="judul"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
            <div id="tampil_modal">
            <!-- Data akan di tampilkan disini-->
            </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
      </div>
    </div>
  </div>
</div>

<script src="plugins/jquery/jquery.min.js"></script>
	<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
	<script>
	  $.widget.bridge('uibutton', $.ui.button)
	</script>
	<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="plugins/chart.js/Chart.min.js"></script>
	<script src="plugins/sparklines/sparkline.js"></script>
	<!-- JQVMap -->
	<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
	<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
	<!-- daterangepicker -->
	<script src="plugins/moment/moment.min.js"></script>
	<script src="plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- Summernote -->
	<script src="plugins/summernote/summernote-bs4.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/adminlte.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="dist/js/pages/dashboard.js"></script>
	<script src="assets/jquery/jquery.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="plugins/jquery-ui/jquery-ui.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="assets/jquery-easing/jquery.easing.min.js"></script>
	<!-- Page level plugin JavaScript-->
	<script src="assets/chart.js/Chart.min.js"></script>
	<script src="assets/datatables/jquery.dataTables.js"></script>
	<script src="assets/datatables/dataTables.bootstrap4.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>
	<!-- Demo scripts for this page-->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/demo/chart-area-demo.js"></script>
	<!-- DataTables  & Plugins -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
	<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
	<script src="plugins/jszip/jszip.min.js"></script>
	<script src="plugins/pdfmake/pdfmake.min.js"></script>
	<script src="plugins/pdfmake/vfs_fonts.js"></script>
	<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
	<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
	<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
	function deleteConfirm(url){
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  function inputTools(){
		var tools = tools;
		const link = '<?php echo base_url('toolsdb/addForm'); ?>';
			$.ajax({
				url: link,
				method: 'post',
				data: {tools:tools},
				success:function(data){
					$('#modal-default').modal("show");
					$('#tampil_modal').html(data);
					document.getElementById("judul").innerHTML='Create tools'; 
				}
            });
    }
  function plusTools(id){
		var id_tools = id;
		const link = '<?php echo base_url('toolsdb/plusForm'); ?>';
			$.ajax({
				url: link,
				method: 'post',
				data: {id_tools:id_tools},
				success:function(data){
					$('#modal-default').modal("show");
					$('#tampil_modal').html(data);
					document.getElementById("judul").innerHTML='Tambah Stock'; 
				}
            });
    }
  function editTools(id){
		var id_tools = id;
		const link = '<?php echo base_url('toolsdb/editForm'); ?>';
			$.ajax({
				url: link,
				method: 'post',
				data: {id_tools:id_tools},
				success:function(data){
					$('#modal-default').modal("show");
					$('#tampil_modal').html(data);
					document.getElementById("judul").innerHTML='Update Data'; 
				}
            });
    }
  function inputKaryawan(){
		var nik = nik;
		const link = '<?php echo base_url('karyawandb/addForm'); ?>';
			$.ajax({
				url: link,
				method: 'post',
				data: {nik:nik},
				success:function(data){
					$('#modal-default').modal("show");
					$('#tampil_modal').html(data);
					document.getElementById("judul").innerHTML='Add Data Karyawan'; 
				}
            });
    }
  function editKaryawan(id){
		var nik = id;
		const link = '<?php echo base_url('karyawandb/editForm'); ?>';
			$.ajax({
				url: link,
				method: 'post',
				data: {nik:nik},
				success:function(data){
					$('#modal-default').modal("show");
					$('#tampil_modal').html(data);
					document.getElementById("judul").innerHTML='Edit Data Karyawan'; 
				}
            });
    }
</script>
</body>
</html>